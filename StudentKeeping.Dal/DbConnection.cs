﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace StudentKeeping.Dal
{
    public class DBConnection
    {
        protected MySqlConnection Connection { get; set; }

        public DBConnection(string connectionName)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            Connection = new MySqlConnection(connectionString);
        }

        public void ChangeConnection(string newConnectionName)
        {
            Connection.ConnectionString = ConfigurationManager
        }
    }
}
