﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentKeeping.Services;

namespace StudentKeeping.Presentation
{
    public partial class Form1 : Form
    {
        private readonly StudentsService studentsService;

        public Form1()
        {
            InitializeComponent();
            studentsService = new StudentsService();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(studentsService.Test());
        }
    }
}
