﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using StudentKeeping.Dal;

namespace StudentKeeping.Services
{
    public class StudentsService : DBConnection
    {
        public StudentsService()
            : base("StudentKeepingConnectionString")
        {}

        public string Test()
        {
            try
            {
                Connection.Open();
                Connection.Close();
                return "Ok";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
